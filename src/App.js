import React from 'react';
import classes from './App.css';
import Router from "./Components/Router/Router";


function App() {
  return (
    <div className={classes.app}>
      <Router/>
    </div>
  );
}

export default App;
