import React from "react"
import classes from "./TextArea.css"

export default (props) => (
    <div className={classes.container}>
        <label className={classes.label}>{props.label}:</label>
        <textarea
            className={classes.input}
            name={props.name}
            id={props.name}
            placeholder={props.placeholder}
            value={props.input || ""}
            onChange={props.Change}
        />
    </div>
)
