import React from "react"
import classes from "./TextInput.css"

export default (props) => (
    <div className={classes.container}>
        <label className={classes.label}>{props.label}</label>
        <input
            className={classes.input}
            type={props.type}
            name={props.name}
            id={props.name}
            placeholder={props.placeholder}
            value={props.input || ""}
            onChange={props.Change}
        />
    </div>
)
