import React from "react";
import classes from "./buttonComponent.css"

export default (props) => {
    let button;
    if (props.light) {
        button = (
            <button onClick={props.onCLick} className={classes.lightButton}>
                {props.name}
            </button>
        )
    } else {
        button = (
            <button onClick={props.onCLick} className={classes.darkButton}>
                {props.name}
            </button>
        )
    }
    return button;
}
