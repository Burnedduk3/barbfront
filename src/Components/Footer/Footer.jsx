import React, {Component} from "react"
import classes from "./Footer.css"
import logo from "./../../assets/Images/Logo.jpg"

class Footer extends Component{
    render() {
        return(
            <footer className={classes.footer}>
                <div>
                    <h1 className={classes.title}>Licenced by BARBATOS INC</h1>
                    <img  src={logo} alt="Logo" className={classes.img}/>
                </div>
                <p className={classes.names}>
                    Pascal Arevalo <br/>
                    Juan David Cabrera <br/>
                    Alberto Combatti <br/>
                    Robert Medina <br/>
                </p>
            </footer>
        )
    }
}

export default Footer
