import React, {Component} from "react"
import classes from "./Nav.css"
import Routes from "../Router/Routes";
import NavbarElement from "../NavbarElement/NavbarElement";
import cookie from "js-cookie";

class Nav extends Component{

    renderNavOptions(){
        let userRoute = Routes.sheared;
        if(this.props.type === "ROLE_ADMIN"){
            userRoute = Routes.government;
        }
        if(this.props.type === "ROLE_USER"){
            userRoute = Routes.patient;
        }
        if(this.props.type === "ROLE_DOCTOR"){
            userRoute = Routes.medic;
        }
        if(this.props.type === "ROLE_EPS_ADMIN"){
            userRoute = Routes.eps;
        }

        return Object.values(userRoute).map(
            (item,index)=>{
                return (
                    <NavbarElement key={index} item={item}/>
                )
            }
        );
    }

    componentDidMount() {
        if(cookie.get("userInfo")){
            const usercookie = cookie.getJSON("userInfo");
            this.setState({
               type: usercookie.user.authorities[usercookie.user.authorities.length-1].roleName
            });
        }
    }

    render() {
        return(
            <nav className={classes.navbar}>
                <ul className={classes.list}>
                    {this.renderNavOptions()}
                </ul>
            </nav>
        )
    }
}

export default Nav;
