export default
{
    home: {
        id: 0,
        path: "/",
        name: "home"
    },
    page404: {
        id: 1,
        path: "/page404",
        name: "Page404"
    },
    sheared: {
        index: {
            id: 2,
            path: "/index",
            name: "index"
        },
        login: {
            id: 3,
            path: "/login",
            name:"Login"
        }
    },
    patient:{
        index:{
            id:1,
            path:"/patient",
            name:"Home"
        },
        appointments:{
            id:2,
            path:"/appointments",
            name:"Citas"
        },
        clinicalHistory:{
            id:3,
            path:"/clinicalHistory",
            name:"Historia Clinica"
        }
    },
    medic:{
        index:{
            id:2,
            path:"/Doctor",
            name:"Home"
        },
        schedule:{
            id:3,
            path:"/schedule",
            name:"Horario"
        },
        patientHistory:{
            id:3,
            path:"/patientHistory",
            name:"Consultar Historia Clinica"
        },
        appointmentRegistry:{
            id:4,
            path:"/appointmentRegistry",
            name:"Registro Consulta"
        },
        availability:{
            id:5,
            path:"/availability",
            name:"Disponibilidad"
        }
    },
    eps:{
        index:{
            id:2,
            path:"/eps",
            name:"Home"
        },
        registry:{
            id:3,
            path:"/registry",
            name:"Resgitro"

        },
        seeContracts:{
            id:4,
            path:"/Contracts",
            name:"Contratos"
        },
        Statistics:{
            id:5,
            path:"/Statistics",
            name:'Estaditsicas'
        }
    },
    government:{
        index:{
            id:2,
            path:"/government",
            name:"Home"
        },
        statistics:{
            id:3,
            path:"/statistics",
            name:"Estadisticas",
        },
        Contracts:{
            id:4,
            path:"/Contracts",
            name:"Contratos",
        },
    }
}
