import React from "react"
import {BrowserRouter as Router, Route, Switch, Redirect} from "react-router-dom";

import RoutesConstants from "./Routes";
import Page404 from "../../Containers/Page404/Page404";
import Home from "../../Containers/Home/Home";

function Routers() {

    return (
        <Router>
            <Switch>
                <Route exact path={RoutesConstants.home.path}
                       render={() => <Redirect to={RoutesConstants.sheared.index.path}/>}/>
                <Route path={RoutesConstants.home.path} component={Home}/>
                <Route path={RoutesConstants.page404.path} component={Page404}/>
                <Redirect to={RoutesConstants.page404.path}/>
            </Switch>
        </Router>
    )
}

export default Routers
