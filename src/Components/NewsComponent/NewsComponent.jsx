import React from "react"
import classes from "./NewsComponent.css"
import classesSmall from "./NewsComponentSmall.css"

export default (props) => {
    let renderComponent;
    if(props.hd){
        renderComponent =
            (
                <>
                    <div className={classes.card}>
                        <img src={props.image} alt={props.alt} className={classes.img}/>
                        <div className={classes.textContainer}>
                            <h1 className={classes.title}>{props.title}</h1>
                            <p className={classes.text}>{props.text}</p>
                        </div>
                    </div>
                </>
            );
    }else{
        renderComponent =
            (
                <>
                    <div className={classesSmall.card}>
                        <img src={props.image} alt={props.alt} className={classesSmall.img}/>
                        <div className={classesSmall.textContainer}>
                            <h1 className={classesSmall.title}>{props.title}</h1>
                            <p className={classesSmall.text}>{props.text}</p>
                        </div>
                    </div>
                </>
            );
    }


    return renderComponent;
}
