import React from "react"
import {Link} from "react-router-dom";
import classes from './NavbarElement.css'

export default (props) => {
    return (
        <>
            <Link to={props.item.path} className={classes.link}>
                <li className={classes.element} key={props.item.id}>{props.item.name}</li>
            </Link>
        </>
    )
}
