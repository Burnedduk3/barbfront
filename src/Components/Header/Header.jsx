import React, {Component} from "react"
import classes from "./Header.css"
import {Link} from "react-router-dom";
import Routes from "../Router/Routes";

class Header extends Component{
    render() {
        return(
                <header className={classes.header}>
                    <h1 className={classes.title}><Link to={Routes.sheared.index.path}>BARBATOS</Link></h1>
                </header>
        )
    }

}

export default Header;
