import React, {Component} from "react"
import classes from "./Appointment.css";
import Button from "../Button/buttonComponent";

export default class appointment extends Component {

    onDelete = () => {
        this.props.onDelete(this.props.calendarId)
    };

    onSelect = () => {
        this.props.onSelectAppointment(this.props.startDate);
        this.props.onDelete(this.props.calendarId);
    };

    render() {
        return (

            <>
                <div className={classes.nextAppointment}>
                    <img className={classes.img}
                         src="https://tse1.mm.bing.net/th?id=OIP.yJwOEW-hy-7UKXWjoMIc1wHaFO&pid=Api"
                         alt="user profile pic"/>
                    <p className={classes.appointmentInfo}>
                        Hora de inicio: {this.props.startDate} <br/>
                        Hora de finalizacion: {this.props.endDate} <br/>
                        Nombre: {this.props.firstName ? this.props.firstName.concat(" " + this.props.lastName) : "No hay usuario postulado aun"}<br/>
                    </p>
                    <div className={classes.buttonContainer}>
                        <Button name={"Iniciar consulta"} light={true} onCLick={this.onSelect}/>
                        <Button name={"Eliminar consulta"} light={true} onCLick={this.onDelete}/>
                    </div>
                </div>
            </>
        )
    }

}
