import React, {Component} from "react"
import Routes from "../../Components/Router/Routes";
import cookie from "js-cookie";
import {Redirect} from "react-router-dom"
import {connect} from 'react-redux';
import * as actionCreators from '../../Store/Actions/User/UserActions';
import * as patientCreators from "./../../Store/Actions/Patient/PatientActions"

import classes from "./MiniMenu.css"


class MiniMenu extends Component {

    handleLogout = async () => {
        this.props.onLogOut(true);
        this.props.cleanStore();
        cookie.remove("userInfo")
    };

    componentDidMount() {
        if(cookie.get("userInfo")){
            const usercookie = cookie.getJSON("userInfo");
            this.props.onLogIn(true, usercookie.user)
        }
    }


    render() {

        let render;
        if(this.props.logedin){
            render = (
                <div className={classes.userContainer}>
                    <input className={classes.btn} type="submit" value="Log out" onClick={this.handleLogout}/>
                </div>
            )
        }else{
            render = (
                <div className={classes.container}>
                        <div className={classes.links}>
                        </div>
                    <Redirect to={Routes.sheared.index.path}/>
                </div>
            );
        }

        return render;
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLogOut: (loggedBoolean) => dispatch(actionCreators.logout(loggedBoolean)),
        onLogIn: (loggedBoolean, user) => dispatch(actionCreators.login(loggedBoolean, user)),
        cleanStore: ()=>dispatch(patientCreators.deleteAll())
    }
};

const mapStateToProps = state => {
    return {
        logedin: state.userStore.logedin,
        user: state.userStore.user,
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(MiniMenu);
