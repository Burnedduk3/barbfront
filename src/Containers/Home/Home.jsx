import React, {Component} from "react";
//Dependencies
import {Route} from "react-router";
import {connect} from "react-redux";

//Own Files
import Routes from "../../Components/Router/Routes";
import Entrie from "../Entrie/Entrie";
import Header from "./../../Components/Header/Header";
import Nav from "../../Components/Nav/Nav";
import Footer from "../../Components/Footer/Footer";
import Login from "../Login/Login"
import MiniMenu from "./../MiniMenu/MiniMenu"

//patient imports
import PatientHome from "./../Patient/Home/Home"
import PatientAppointment from "./../Patient/Appointments/Appointment"
import ClinicalHistory from "../Patient/ClinicalHistory/ClinicalHistory";

//Medic Imports
import DoctorHome from "./../Doctor/Home/Home"
import DoctorSchedule from "../Doctor/seeAvailability/seeAvailability"
import DoctorRegistry from "./../Doctor/AddClinicalHistory/AddClinicalHistory"
import DoctorHistoric from "./../Doctor/ClinicalHistory/ClinicalHistory"
import DoctorAvailability from "../Doctor/addAvailavility/addAvailability"

//Government Imports
import GovernmentHome from "./../Ministerio/Index/index"
import GovernmentEstatistics from "./../Ministerio/Statistics/Statistics"
import GovernmentContracts from "./../Ministerio/Contracts/Contracts"

//EPS Imports
import EPSHome from "./../EPS/Index/Index"
import EPSRegistry from "./../EPS/Registry/Registry"
import EPSStatistics from "./../EPS/Statistics/Statistics"
import EPSClinical from "./../EPS/ClinicalHistory/ClinicalHistory"


class Home extends Component {

    renderPatientRoutes() {
        if(this.props.user){
            if (this.props.user.authorities[this.props.user.authorities.length-1].roleName === "ROLE_USER") {// Redux state to see what type of user is
                return (
                    <>
                        <Route path={Routes.patient.index.path} component={PatientHome}/>
                        <Route path={Routes.patient.appointments.path} component={PatientAppointment}/>
                        <Route path={Routes.patient.clinicalHistory.path} component={ClinicalHistory}/>
                    </>
                );
            }
        }
    }

    renderDoctorRoutes() {
        if(this.props.user){
            if (this.props.user.authorities[this.props.user.authorities.length-1].roleName === "ROLE_DOCTOR") {
                return (
                    <>
                        <Route path={Routes.medic.index.path} component={DoctorHome}/>
                        <Route path={Routes.medic.schedule.path} component={DoctorSchedule}/>
                        <Route path={Routes.medic.appointmentRegistry.path} component={DoctorRegistry}/>
                        <Route path={Routes.medic.patientHistory.path} component={DoctorHistoric}/>
                        <Route path={Routes.medic.availability.path} component={DoctorAvailability}/>

                    </>
                );
            }
        }
    }

    renderMinisteriosRoutes() {
        if(this.props.user){
            if (this.props.user.authorities[this.props.user.authorities.length-1].roleName === "ROLE_ADMIN") {
                return (
                    <>
                        <Route path={Routes.government.index.path} component={GovernmentHome}/>
                        <Route path={Routes.government.statistics.path} component={GovernmentEstatistics}/>
                        <Route path={Routes.government.Contracts.path} component={GovernmentContracts}/>
                    </>
                );
            }
        }
    }

    renderEPSRoutes() {
        if(this.props.user){
            if (this.props.user.authorities[this.props.user.authorities.length-1].roleName === "ROLE_EPS_ADMIN") {
                return (
                    <>
                        <Route path={Routes.eps.index.path} component={EPSHome}/>
                        <Route path={Routes.eps.registry.path} component={EPSRegistry}/>
                        <Route path={Routes.eps.Statistics.path} component={EPSStatistics}/>
                        <Route path={Routes.eps.seeContracts.path} component={EPSClinical}/>
                    </>
                );
            }
        }
    }

    render() {
        let type ="";
        this.props.user ? type = this.props.user.authorities[this.props.user.authorities.length-1].roleName: type = "";
        return (
            <>
                <Header/>
                <Nav type={type}/>
                <MiniMenu/>
                {this.renderPatientRoutes()}
                {this.renderDoctorRoutes()}
                {this.renderMinisteriosRoutes()}
                {this.renderEPSRoutes()}
                <Route path={Routes.sheared.index.path} component={Entrie}/>
                <Route path={Routes.sheared.login.path} component={Login}/>
                <Footer/>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        logedin: state.userStore.logedin,
        user: state.userStore.user
    }
};

export default connect(mapStateToProps)(Home);
