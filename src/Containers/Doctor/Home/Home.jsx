import React, {Component} from "react";
import classes from "./Home.css";
import {connect} from "react-redux";
import API from "./../../../API";
import * as actionCreators from "../../../Store/Actions/Doctor/DoctorActions";

class Home extends Component {


    async fetchAppointments() {
        if(this.props.appointments.length <=0) {
            const response = await API.get("/doctor/calendar/".concat(this.props.user.identificationCard + "/"), {
                auth: {
                    username: this.props.user.username,
                    password: this.props.user.password
                }
            });
            response.data.map((item) => (this.props.onAddAppointment(item)))
        }
    }

    componentDidMount() {
        this.fetchAppointments()
    }

    renderNextAppointment() {
        if (this.props.appointments.length > 0) {
            return (
                <div className={classes.appointmens}>
                    <p>Appointments Left: {this.props.appointments.length} </p>
                    <div className={classes.nextAppointment}>
                        <img src="https://tse1.mm.bing.net/th?id=OIP.yJwOEW-hy-7UKXWjoMIc1wHaFO&pid=Api"
                             alt="user profile pic"/>
                        <p className={classes.appointmentInfo}>
                            Hora de inicio: {this.props.appointments[0].appointment.startDate} <br/>
                            Hora de finalizacion: {this.props.appointments[0].appointment.endDate} <br/>
                            Nombre: {this.props.appointments[0].appointment.patient ? this.props.appointments[0].appointment.patient.firstName.concat(" ").concat(this.props.appointments[0].appointment.patient.lastName) : "No hay usuario postulado"}<br/>
                        </p>
                    </div>
                </div>
            )
        } else {
            return (
                <div className={classes.appointmens}>
                    <h2>No hay citas hasta el momento</h2>
                </div>
            )
        }
    }

    render() {
        return (
            <div className={classes.container}>
                <div className={classes.doctorInfo}>
                    <img className={classes.img}
                         src="https://randomletterstotheworld.files.wordpress.com/2013/07/infection-doctor.jpg"
                         alt="profile"/>
                    <div className={classes.textCont}>
                        <h2 className={classes.name}>
                            Bienvenido Doctor: {this.props.user.firstName} {this.props.user.lastName}
                        </h2>
                        <p className={classes.doctorInfopara}>
                            Telefono: {this.props.user.phone} <br/>
                            Direccion: {this.props.user.address} <br/>
                            Usuario: {this.props.user.username} <br/>
                        </p>
                    </div>
                </div>
                {this.renderNextAppointment()}
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAddAppointment: (appointment) => dispatch(actionCreators.addApointment(appointment)),
    }
};

const mapStateToProps = state => {
    return {
        appointments: state.doctorStore.appointments,
        user: state.userStore.user
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
