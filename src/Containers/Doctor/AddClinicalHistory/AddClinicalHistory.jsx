import React, {Component} from "react";
import classes from "./AddClinicalHistory.css"
import Spinner from "../../../Components/Spinner/Spinner";
import Button from "../../../Components/Button/buttonComponent";
import TextArea from "../../../Components/TextArea/TextArea";
import TextInput from "../../../Components/TextInput/TextInput";
import API from "../../../API";
import {connect} from "react-redux";

class AddClinicalHistory extends Component {
    state = {
        inputs: {
            reasonForConsultation: "",
            currentDisease: "",
            identificationCard: "",
        },
        error: false,
        loading: false
    };

    async addHistory() {
        try {
            this.setState({...this.state, loading: true});
            const response = await API.get("/patient/history/".concat(this.state.inputs.patientID), {
                auth: {
                    username: this.props.user.username,
                    password: this.props.user.password
                }
            });

            if (response.data === "") {
                const addHistory = await API.post("/doctor/appointment/".concat(this.state.inputs.identificationCard + "/"),
                    {
                        reasonForConsultation: this.state.inputs.reasonForConsultation,
                        currentDisease: this.state.inputs.currentDisease,
                        patient: {
                            identificationCard: this.state.inputs.identificationCard
                        }

                    },
                    {
                        auth: {
                            username: this.props.user.username,
                            password: this.props.user.password
                        },
                        headers: {
                            'Content-Type': "application/json"
                        }
                    }
                );
                console.log(addHistory)
            } else {
                const addHistory = await API.put("/doctor/appointment/".concat(this.state.inputs.identificationCard + "/"),
                    {
                        reasonForConsultation: this.state.inputs.reasonForConsultation,
                        currentDisease: this.state.inputs.currentDisease,
                        patient: {
                            identificationCard: this.state.inputs.identificationCard
                        }

                    },
                    {
                        auth: {
                            username: this.props.user.username,
                            password: this.props.user.password
                        },
                        headers: {
                            'Content-Type': "application/json"
                        }
                    }
                );
                console.log(addHistory)
            }
            this.setState({...this.state, loading: false});
        } catch (e) {
            console.log(e)
            this.setState({...this.state, loading: false, error: true});
        }
    }

    onClick = () => {
        this.addHistory();
    };

    renderButton() {
        if (this.state.loading) {
            return <Spinner/>
        } else {
            return <Button name={"Añadir historia Clinica"} onCLick={this.onClick}/>
        }
    }

    renderError() {
        if (this.state.error) {
            return (
                <div className={classes.error}>
                    <h2>Ha ocurrido un error, Revise los campos.</h2>
                </div>
            )
        }
    }

    handleChange = (event) => {
        let reasonForConsultation = "", currentDisease = "", identificationCard = "";

        if (event.target.name === "reasonForConsultation") {
            reasonForConsultation += event.target.value;
            this.setState({
                ...this.state,
                inputs: {
                    ...this.state.inputs,
                    reasonForConsultation: reasonForConsultation,
                }
            });
        }

        if (event.target.name === "currentDisease") {
            currentDisease += event.target.value;
            this.setState({
                ...this.state,
                inputs: {
                    ...this.state.inputs,
                    currentDisease: currentDisease,
                }
            });
        }

        if (event.target.name === "idCard") {
            identificationCard += event.target.value;
            this.setState({
                ...this.state,
                inputs: {
                    ...this.state.inputs,
                    identificationCard: identificationCard,
                }
            });
        }
    };

    render() {
        let {reasonForConsultation, currentDisease, identificationCard} = this.state.inputs;
        return (
            <div className={classes.container}>
                <h2 className={classes.title}>Registrar Consulta</h2>
                <div className={classes.inputArea}>
                    <TextInput
                        label={"Cedula"}
                        type={"text"}
                        name={"idCard"}
                        placeholder={"1072669885"}
                        input={identificationCard} Change={this.handleChange}
                    />
                    <TextArea label={"Razon De Consulta"}
                              type={"text"}
                              name={"reasonForConsultation"}
                              placeholder={"Tiene dolor de cabeza"}
                              input={reasonForConsultation} Change={this.handleChange}
                    />
                    <TextArea label={"Diagnostico"}
                              type={"text"}
                              name={"currentDisease"}
                              placeholder={"Diagnostico"}
                              input={currentDisease} Change={this.handleChange}
                    />
                </div>
                <div className={classes.btn}>
                    {this.renderButton()}
                </div>
                {this.renderError()}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.userStore.user
    }
};

export default connect(mapStateToProps)(AddClinicalHistory);
