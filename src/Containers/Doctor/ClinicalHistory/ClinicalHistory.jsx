import React, {Component} from "react"
import API from "./../../../API"
import {connect} from "react-redux";
import classes from "./ClinicalHistory.css"
import Spinner from "../../../Components/Spinner/Spinner";
import TextInput from "../../../Components/TextInput/TextInput";
import Button from "../../../Components/Button/buttonComponent";

class ClinicalHistory extends Component {
    state = {
        inputs: {
            patientID: "",
        },
        userHistoric: "",
        loading: false,
        error: false,
    };

    async fetchClinicalHistory() {
        try {
            this.setState({...this.state, loading: true});
            const response = await API.get("/patient/history/".concat(this.state.inputs.patientID), {
                auth: {
                    username: this.props.user.username,
                    password: this.props.user.password
                }
            });
            this.setState({...this.state, loading: false, userHistoric: response.data})
        } catch (e) {
            console.log(e);
            this.setState({...this.state, error: true, loading: false})
        }
    }

    renderHistoric() {
        if (this.state.loading) {
            return <Spinner/>
        } else {
            if (this.state.error) {
                return <p className={classes.error}>Hubo un error al traer la informacion</p>
            } else {
                if (this.state.userHistoric !== "") {
                    return <p className={classes.historyText}>{this.state.userHistoric}</p>
                } else {
                    return <p className={classes.historyText}>No hay registros todavia</p>

                }
            }
        }
    }

    handleChange = (event) => {
        let patientID = "";
        if (event.target.name === "idCard") {
            patientID += event.target.value;
            this.setState({
                ...this.state,
                inputs: {
                    ...this.state.inputs,
                    patientID: patientID,
                }
            });
        }
    };

    onClick = () => {
        this.fetchClinicalHistory()
    };

    render() {
        return (
            <div className={classes.Container}>
                <h2 className={classes.title}>Historias Clinicas</h2>
                <div className={classes.History}>
                    <TextInput
                        label={"Cedula"}
                        type={"text"}
                        name={"idCard"}
                        placeholder={"1072669885"}
                        input={this.state.inputs.patientID} Change={this.handleChange}
                    />
                    {this.renderHistoric()}
                </div>
                <Button name={"Buscar Paciente"} light={false} onCLick={this.onClick}/>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.userStore.user
    }
};

export default connect(mapStateToProps)(ClinicalHistory);
