import React, {Component} from "react";
import classes from "./Disponibility.css"
import {DatePicker} from '@y0c/react-datepicker';
import '@y0c/react-datepicker/assets/styles/calendar.scss';
import 'moment/locale/es';
import API from "./../../../API"
import {connect} from "react-redux";
import * as actionCreators from "../../../Store/Actions/Doctor/DoctorActions";
import Appointment from "./../../../Components/Appointment/Appointment";


class SeeAvailability extends Component {
    state = {
        inputs: {
            startDate: "",
            endDate: ""
        },
        loading: false,
        error: false
    };


    async fetchAppointments() {
        try {
            if (this.props.appointments.length === 0) {
                const response = await API.get("/doctor/calendar/".concat(this.props.user.identificationCard + "/"), {
                    auth: {
                        username: this.props.user.username,
                        password: this.props.user.password
                    }
                });
                response.data.map((item) => (this.props.onAddAppointment(item)));
            } else {
                if (this.state.inputs.startDate !== "" || this.state.inputs.endDate !== "") {
                    const response = await API.get("/doctor/calendar/".concat(this.props.user.identificationCard + "/"), {
                        params: {
                            startDate: this.state.inputs.startDate,
                            endDate: this.state.inputs.endDate
                        },
                        auth: {
                            username: this.props.user.username,
                            password: this.props.user.password
                        }
                    });
                    this.props.onDeleteAll();
                    response.data.map((item) => (this.props.onAddAppointment(item)));
                }
            }
        } catch (e) {
            console.log(e)
        }
    }

    componentDidMount() {
        this.fetchAppointments()
    }

    onSelectStart = (date) => {
        const year = date.$y, month = date.$M + 1, day = "".concat(date.$d).split(" ")[2],
            hour = "".concat(date.$d).split(" ")[4].split(":")[0], min = "".concat(date.$d).split(" ")[4].split(":")[1];
        const dateStart = year + "-" + month + "-" + day + "-" + hour + ":" + min + ":00";
        this.setState({
            ...this.state,
            inputs: {
                ...this.state.inputs,
                startDate: dateStart
            }
        })
        this.fetchAppointments();
    };

    onSelectEnd = (date) => {
        const year = date.$y, month = date.$M + 1, day = "".concat(date.$d).split(" ")[2],
            hour = "".concat(date.$d).split(" ")[4].split(":")[0], min = "".concat(date.$d).split(" ")[4].split(":")[1];
        const dateEnd = year + "-" + month + "-" + day + "-" + hour + ":" + min + ":00";

        this.setState({
            ...this.state,
            inputs: {
                ...this.state.inputs,
                endDate: dateEnd
            }
        });
        this.fetchAppointments();
    };

    renderAppointments() {
        if (this.props.appointments.length > 0) {
            return this.props.appointments.map(
                (item) => {
                    return (
                        < Appointment
                            key={item.appointment.calendarId}
                            startDate={item.appointment.startDate}
                            endDate={item.appointment.endDate}
                            firstName={item.appointment.patient ? item.appointment.patient.firstName : ""}
                            lastName={item.appointment.patient ? item.appointment.patient.lastName : ""}
                            onDelete={this.props.onDeleteAppointment}
                            calendarId={item.appointment.calendarId}
                        />
                    )
                }
            )
        } else {
            return <h2 className={classes.noAppointment}>No hay citas registradas aun</h2>
        }
    }

    render() {
        return (
            <div className={classes.Container}>
                <div className={classes.dates}>
                    <p className={classes.title}>Fecha de Inicio</p>
                    <DatePicker onChange={this.onSelectStart} includeTime/>
                    <p className={classes.title}>Fecha de Cierre</p>
                    <DatePicker onChange={this.onSelectEnd} includeTime/>
                </div>
                <div className={classes.appointmentsContainer}>
                    {this.renderAppointments()}
                </div>
            </div>
        )
    }

}

const mapDispatchToProps = dispatch => {
    return {
        onAddAppointment: (appointment) => dispatch(actionCreators.addApointment(appointment)),
        onDeleteAppointment: (appointment) => dispatch(actionCreators.deleteAppointment(appointment)),
        onDeleteAll: () => dispatch(actionCreators.deleteAll())
    }
};

const mapStateToProps = state => {
    return {
        appointments: state.doctorStore.appointments,
        user: state.userStore.user
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SeeAvailability);
