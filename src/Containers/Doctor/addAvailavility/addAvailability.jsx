import React, {Component} from "react"
import classes from "./Availability.css"
import TextArea from "../../../Components/TextArea/TextArea";
import {DatePicker} from '@y0c/react-datepicker';
import '@y0c/react-datepicker/assets/styles/calendar.scss';
import 'moment/locale/es';
import Button from "./../../../Components/Button/buttonComponent"
import API from "./../../../API"
import {connect} from "react-redux";
import Spinner from "../../../Components/Spinner/Spinner";


class AddAvailability extends Component {
    state = {
        inputs: {
            description: "",
            startDate: "dd-MM-yyyy HH:mm:ss",
            endDate: "dd-MM-yyyy HH:mm:ss",
        },
        error: false,
        loading: false
    };


    handleChange = (event) => {
        let description = "";

        if (event.target.name === "Description") {
            description += event.target.value;
            this.setState({
                ...this.state,
                inputs: {
                    ...this.state.inputs,
                    description: description,
                }
            });
        }
    };

    onSelectStart = (date) => {
        const year = date.$y, month = date.$M + 1, day = "".concat(date.$d).split(" ")[2],
            hour = "".concat(date.$d).split(" ")[4].split(":")[0], min = "".concat(date.$d).split(" ")[4].split(":")[1];
        const dateStart = `${year}-${month}-${day} ${hour}:${min}:00`;
        this.setState({
            ...this.state,
            inputs: {
                ...this.state.inputs,
                startDate: dateStart
            }
        })
    };

    onSelectEnd = (date) => {
        const year = date.$y, month = date.$M + 1, day = "".concat(date.$d).split(" ")[2],
            hour = "".concat(date.$d).split(" ")[4].split(":")[0], min = "".concat(date.$d).split(" ")[4].split(":")[1];
        const dateEnd = `${year}-${month}-${day} ${hour}:${min}:00`;

        this.setState({
            ...this.state,
            inputs: {
                ...this.state.inputs,
                endDate: dateEnd
            }
        })
    };

    async sendToDatabase() {
        try {
            this.setState({...this.state, loading: true});
            await API.post("/doctor/calendar", {
                    description: this.state.inputs.description,
                    startDate: this.state.inputs.startDate,
                    endDate: this.state.inputs.endDate,
                },
                {
                    auth: {
                        username: this.props.user.username,
                        password: this.props.user.password
                    },
                    headers: {
                        'Content-Type': "application/json"
                    }
                }
            );
            this.setState({
                inputs: {
                    description: "",
                    startDate: "dd-MM-yyyy HH:mm:ss",
                    endDate: "dd-MM-yyyy HH:mm:ss",
                },
                error: false,
                loading: false
            });

        } catch (e) {
            this.setState({...this.state, error: true, loading: false});
        }
    }

    onClick = () => {
        this.sendToDatabase();
    };

    renderButton() {
        if (this.state.loading) {
            return <Spinner/>
        } else {
            return <Button name={"Añadir disponibilidad"} onCLick={this.onClick}/>
        }
    }

    renderError() {
        if (this.state.error) {
            return (
                <div className={classes.error}>
                    <h2>Ha ocurrido un error, Revise los campos. Revise que la cita no dure mas de 30 mins</h2>
                </div>
            )
        }
    }

    render() {
        const {description} = this.state.inputs;
        return (
            <div className={classes.container}>
                <div className={classes.dates}>
                    <p className={classes.title}>Fecha de Inicio</p>
                    <DatePicker onChange={this.onSelectStart} includeTime/>
                    <p className={classes.title}>Fecha de cierre</p>
                    <DatePicker onChange={this.onSelectEnd} includeTime/>
                </div>
                <TextArea label={"Description"}
                          type={"text"}
                          name={"Description"}
                          placeholder={"Descriptio"}
                          input={description} Change={this.handleChange}
                />
                <br/>
                {this.renderButton()}
                {this.renderError()}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.userStore.user
    }
};

export default connect(mapStateToProps)(AddAvailability);
