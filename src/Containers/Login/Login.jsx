import React, {Component} from "react"
import classes from "./Login.css"
import Button from "./../../Components/Button/buttonComponent"
import TextInput from "../../Components/TextInput/TextInput";
import * as actionCreators from "../../Store/Actions/User/UserActions";
import {connect} from "react-redux";
import API from "./../../API";
import cookie from "js-cookie";
import {Redirect} from "react-router";
import Routes from "../../Components/Router/Routes";
import Spinner from "../../Components/Spinner/Spinner";


class Login extends Component {
    state = {
        inputs: {
            userInput: "",
            passwordInput: ""
        },
        loading:false,
        redirect: false,
        error:false
    };

    handleChange = (event) => {
        let userInput = "", inputPassword = "";
        if (event.target.name === "username") {
            userInput += event.target.value;
            this.setState({
                ...this.state,
                inputs: {
                    ...this.state.inputs,
                    userInput: userInput,
                }
            });
        }

        if (event.target.name === "password") {
            inputPassword += event.target.value;
            this.setState({
                ...this.state,
                inputs: {
                    ...this.state.inputs,
                    passwordInput: inputPassword
                }
            });
        }
    };

    async getUser() {
        try {
            this.setState({...this.state, loading:true})
            const response = await API.get("/user/profile/", {
                auth: {
                    username: this.state.inputs.userInput,
                    password: this.state.inputs.passwordInput
                }
            });
            cookie.set("userInfo",
                {
                    user: {
                        ...response.data,
                        password: this.state.inputs.passwordInput
                    }
                });
            response.data.password = this.state.inputs.passwordInput;
            this.props.onLogIn(true, response.data);
            this.setState({
                ...this.state,
                redirect: true,
                user: response.data
            })
            this.setState({...this.state,loading:true})
        } catch (e) {
            console.log(e)
            this.setState({
                ...this.state,
                error:true,
                loading:false
            })
        }
    }

    onClick = () => {
        this.getUser();
    };

    renderError(){
        if(this.state.error){
            return(
                <div className={classes.error}>
                    <p>Usuario o contraseña invalidos, verifique</p>
                </div>
            )
        }
    }

    handleRedirect(){
        if(this.state.user){
            if(this.state.user.authorities[this.state.user.authorities.length-1].roleName === "ROLE_DOCTOR"){

                return <Redirect to={Routes.medic.index.path}/>
            }

            if(this.state.user.authorities[this.state.user.authorities.length-1].roleName === "ROLE_USER"){
                return <Redirect to={Routes.patient.index.path}/>
            }

            if(this.state.user.authorities[this.state.user.authorities.length-1].roleName === "ROLE_ADMIN"){
                return <Redirect to={Routes.government.index.path}/>
            }

            if(this.state.user.authorities[this.state.user.authorities.length-1].roleName === "ROLE_EPS_ADMIN"){
                return <Redirect to={Routes.eps.index.path}/>
            }
        }
    }

    renderButton(){
        if(this.state.loading){
            return <Spinner/>
        }else{
            return <Button name={"Log In"} light={false} onCLick={this.onClick}/>

        }
    }


    render() {
        const {handleChange} = this;
        return (
            <div className={classes.loginContainer}>
                <div className={classes.loginCard}>
                    <TextInput label={"Username:"} type={"text"} name={"username"} placeholder={"Username"}
                               input={this.state.inputs.userInput} Change={handleChange}/>
                    <TextInput label={"Password:"} type={"password"} name={"password"} placeholder={"Password"}
                               input={this.state.inputs.passwordInput} Change={handleChange}/>
                    <div className={classes.button}>
                        {this.renderButton()}
                    </div>
                </div>
                {this.handleRedirect()}
                {this.renderError()}
            </div>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        onLogIn: (loggedBoolean, user) => dispatch(actionCreators.login(loggedBoolean, user)),
    }
};

const mapStateToProps = state => {
    return {
        logedin: state.userStore.logedin,
        user: state.userStore.user
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
