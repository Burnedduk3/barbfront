import React, {Component} from "react";
import classes from "./Registry.css";
import TextInput from "../../../Components/TextInput/TextInput";
import Button from "./../../../Components/Button/buttonComponent";
import Spinner from "../../../Components/Spinner/Spinner";
import API from "./../../../API"

class Registry extends Component {
    state = {
        inputs: {
            username: "",
            password: "",
            firstName: "",
            lastname: "",
            phone: "",
            address: "",
            personalID: ""
        },
        loading: false,
        error: false,
        redirect: false
    };

    handleChange = (event) => {
        let username = "", password = "", firstname = "", lastname = "", phone = "", address = "", personalID="";
        if (event.target.name === "Username") {
            username += event.target.value;
            this.setState({
                ...this.state,
                inputs: {
                    ...this.state.inputs,
                    username: username,
                }
            });
        }

        if (event.target.name === "password") {
            password += event.target.value;
            this.setState({
                ...this.state,
                inputs: {
                    ...this.state.inputs,
                    password: password,
                }
            });
        }

        if (event.target.name === "firstName") {
            firstname += event.target.value;
            this.setState({
                ...this.state,
                inputs: {
                    ...this.state.inputs,
                    firstName: firstname,
                }
            });
        }

        if (event.target.name === "lastname") {
            lastname += event.target.value;
            this.setState({
                ...this.state,
                inputs: {
                    ...this.state.inputs,
                    lastname: lastname,
                }
            });
        }

        if (event.target.name === "Phone") {
            phone += event.target.value;
            this.setState({
                ...this.state,
                inputs: {
                    ...this.state.inputs,
                    phone: phone,
                }
            });
        }

        if (event.target.name === "address") {
            address += event.target.value;
            this.setState({
                ...this.state,
                inputs: {
                    ...this.state.inputs,
                    address: address,
                }
            });
        }

        if(event.target.name === "personalID"){
            personalID += event.target.value;
            this.setState({
                ...this.state,
                inputs: {
                    ...this.state.inputs,
                    personalID: personalID
                }
            });
        }
    };

    async sendUser() {
        try {
            this.setState({...this.state, loading: true});
            await API.post('/user/register', {
                identificationCard: this.state.inputs.personalID,
                username: this.state.inputs.username,
                password: this.state.inputs.password,
                firstName: this.state.inputs.firstName,
                lastName: this.state.inputs.lastname,
                phone: this.state.inputs.phone,
                address: this.state.inputs.address

            }, {
                headers: {
                    'Content-Type': "application/json"
                }
            });
            this.setState({
                inputs: {
                    username: "",
                    password: "",
                    firstName: "",
                    lastname: "",
                    phone: "",
                    address: "",
                    personalID:""
                },
                error: false,
                loading: false
            });
        } catch (e) {
            console.log(e);
            this.setState({...this.state, error: true, loading: false});
        }
    }

    onClick = () => {
        this.sendUser();
    };

    renderButton() {
        if (this.state.loading) {
            return <Spinner/>
        } else {
            return <Button name={"Submit"} onCLick={this.onClick}/>
        }
    }

    renderError(){
        if(this.state.error){
            return <p>Hay un error, revisa que todos los campos esten completos.</p>
        }
    }

    render() {
        let {username, password, lastname, firstName, phone, address, personalID} = this.state.inputs;
        return (
            <div className={classes.container}>
                <div className={classes.InputContainer}>
                    <TextInput label={"Cedula"}
                               type={"text"}
                               name={"personalID"}
                               placeholder={"1072669885"}
                               input={personalID} Change={this.handleChange}
                    />
                    <TextInput label={"Usuario"}
                               type={"text"}
                               name={"Username"}
                               placeholder={"Nombre de usuario"}
                               input={username} Change={this.handleChange}
                    />
                    <TextInput label={"Contraseña"}
                               type={"password"}
                               name={"password"}
                               placeholder={"Contraseña"}
                               input={password} Change={this.handleChange}
                    />
                    <TextInput label={"Nombre"}
                               type={"text"}
                               name={"firstName"}
                               placeholder={"Primer Nombre"}
                               input={firstName} Change={this.handleChange}
                    />
                    <TextInput label={"Apellidos"}
                               type={"text"}
                               name={"lastname"}
                               placeholder={"Apellidos"}
                               input={lastname} Change={this.handleChange}
                    />
                    <TextInput label={"Telefono"}
                               type={"text"}
                               name={"Phone"}
                               placeholder={"123 123 1234"}
                               input={phone} Change={this.handleChange}
                    />
                    <TextInput label={"Direccion"}
                               type={"text"}
                               name={"address"}
                               placeholder={"cra 1 # 34a - 34"}
                               input={address} Change={this.handleChange}
                    />
                </div>
                <div className={classes.buttonContainer}>
                    {this.renderButton()}
                </div>
                <div className={classes.error}>
                    {this.renderError()}
                </div>
            </div>
        );
    }
}

export default Registry;
