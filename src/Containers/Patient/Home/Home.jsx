import React, {Component} from "react";
import classes from "./Home.css";
import {connect} from "react-redux";

class Home extends Component {

    render() {
        return (
            <div className={classes.container}>
                <div className={classes.patientInfo}>
                    <img className={classes.img}
                         src="https://www.recreoviral.com/wp-content/uploads/2016/12/pecas6.jpg"
                         alt="profile"/>
                    <div className={classes.textCont}>
                        <h2 className={classes.name}>
                            Bienvenido: {this.props.user.firstName} {this.props.user.lastName}
                        </h2>
                        <p className={classes.patientInfoParagraph}>
                            Telefono: {this.props.user.phone} <br/>
                            Direccion: {this.props.user.address} <br/>
                            Usuario: {this.props.user.username} <br/>
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
    }
};

const mapStateToProps = state => {
    return {
        user: state.userStore.user
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
