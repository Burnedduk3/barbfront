import React, {Component} from "react"
import API from "./../../../API"
import {connect} from "react-redux";
import classes from "./ClinicalHistory.css"
import Spinner from "../../../Components/Spinner/Spinner";

class ClinicalHistory extends Component {
    state = {
        userHistoric: "",
        loading: false,
        error: false,
    };

    async fetchClinicalHistory() {
        try {
            this.setState({...this.state, loading: true});
            const response = await API.get("/patient/history/", {
                auth: {
                    username: this.props.user.username,
                    password: this.props.user.password
                }
            });
            this.setState({...this.state, loading: false, userHistoric: response.data})
        } catch (e) {
            console.log(e)
            this.setState({...this.state, error: true, loading: false})
        }
    }

    componentDidMount() {
        this.fetchClinicalHistory()
    }

    renderHistoric() {
        if (this.state.loading) {
            return <Spinner/>
        } else {
            if (this.state.error) {
                return <p className={classes.error}>Hubo un error al traer la informacion</p>
            } else {
                if (this.state.userHistoric !== "") {
                    console.log(this.state);
                    return this.state.userHistoric.map((item, index) => {
                        return item.medical_history.map((registry, index)=>{
                            console.log(registry)
                            return (
                                <>
                                    <p className={classes.historyText}>{registry.currentDisease}</p>
                                    <br/>
                                    <p className={classes.historyText}>{registry.reasonForConsultation}</p>
                                </>
                            )
                        })
                    })
                } else {
                    return <p className={classes.historyText}>No hay registros todavia</p>

                }
            }
        }
    }

    render() {
        return (
            <div className={classes.Container}>
                <h2 className={classes.title}>Historias Clinicas</h2>
                <div className={classes.History}>
                    {this.renderHistoric()}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.userStore.user
    }
};

export default connect(mapStateToProps)(ClinicalHistory);

