import React, {Component} from "react"
import API from "../../../API";
import classes from "./Appointment.css";
import {DatePicker} from "@y0c/react-datepicker";
import * as actionCreators from "../../../Store/Actions/Patient/PatientActions";
import {connect} from "react-redux";
import TextInput from "../../../Components/TextInput/TextInput";
import Spinner from "../../../Components/Spinner/Spinner";
import Button from "../../../Components/Button/buttonComponent";
import AppointmentCard from "../../../Components/Appointment/Appointment";

class Appointment extends Component {
    state = {
        inputs: {
            startDate: "",
            endDate: "",
            idCard: ""
        },
        loading: false,
        error: false
    };

    async fetchAppointments() {
        try {
            this.setState({
                ...this.state,
                loading: true
            });
            const response = await API.get("/doctor/calendar/".concat(this.state.inputs.idCard !== "" ? this.state.inputs.idCard : `0/`),
                {
                    params: {
                        startDate: this.state.inputs.startDate,
                        endDate: this.state.inputs.endDate
                    },
                    auth: {
                        username: this.props.user.username,
                        password: this.props.user.password
                    }
                });
            this.props.onDeleteAll();
            response.data.map(
                (item) => {
                    this.props.onAddAppointment(item);
                    return 0
                }
            );

            this.setState({
                ...this.state,
                error: false,
                loading: false
            })
        } catch (e) {
            console.log(e);
            this.setState({
                ...this.state,
                error: true,
                loading: false
            })
        }
    }

    onSelectStart = (date) => {
        const year = date.$y, month = date.$M + 1, day = "".concat(date.$d).split(" ")[2],
            hour = "".concat(date.$d).split(" ")[4].split(":")[0], min = "".concat(date.$d).split(" ")[4].split(":")[1];
        const dateStart = year + "-" + month + "-" + day + "-" + hour + ":" + min + ":00";
        this.setState({
            ...this.state,
            inputs: {
                ...this.state.inputs,
                startDate: dateStart
            }
        })
    };

    onSelectEnd = (date) => {
        const year = date.$y, month = date.$M + 1, day = "".concat(date.$d).split(" ")[2],
            hour = "".concat(date.$d).split(" ")[4].split(":")[0], min = "".concat(date.$d).split(" ")[4].split(":")[1];
        const dateEnd = year + "-" + month + "-" + day + "-" + hour + ":" + min + ":00";

        this.setState({
            ...this.state,
            inputs: {
                ...this.state.inputs,
                endDate: dateEnd
            }
        });
    };

    renderAppointments() {
        if (this.props.appointments) {

            if (this.props.appointments.length > 0) {
                return (this.props.appointments.map(
                        (item) => (
                            <AppointmentCard
                                key={item.appointment.calendarId}
                                startDate={item.appointment.startDate}
                                endDate={item.appointment.endDate}
                                firstName={item.appointment.patient ? item.appointment.patient.firstName : ""}
                                lastName={item.appointment.patient ? item.appointment.patient.lastName : ""}
                                onDelete={this.props.onDeleteAppointment}
                                calendarId={item.appointment.calendarId}
                                onSelectAppointment={this.selectAppointment}
                            />
                        )
                    )
                );
            } else {
                return <h2 className={classes.noAppointment}>No se encontraron citas</h2>
            }
        }
    }

    selectAppointment = (startDate) => {
        console.log(startDate);
        console.log(this.state.inputs.idCard);
        this.sendToDB(startDate);
    };

    async sendToDB(startDate){
        try{
            this.setState({
                ...this.state,
                loading: true
            });

            const response = await API.post("/patient/appointment/",
                {
                    startDate:startDate,
                    doctorIdentificationCard:this.state.inputs.idCard
                },
                {
                    auth:{
                        username: this.props.user.username,
                        password: this.props.user.password
                    },
                    headers: {
                        'Content-Type': "application/json"
                    }
                }
            );

            console.log(response)
            this.setState({
                ...this.state,
                error: false,
                loading: false
            })
        }catch (e) {
            console.log(e)
            this.setState({
                ...this.state,
                error: true,
                loading: false
            })
        }
    }

    handleChange = (event) => {
        let idCard = "";
        if (event.target.name === "idCard") {
            idCard += event.target.value;
            this.setState({
                ...this.state,
                inputs: {
                    ...this.state.inputs,
                    idCard: idCard,
                }
            });
        }
    };

    renderError() {
        if (this.state.error) {
            return (
                <div className={classes.errorContainer}>
                    <h2 className={classes.error}>Revise que alla dijitado los numeros de la cedula bien.</h2>
                </div>
            )
        }
    }

    onClick = () => {
        this.fetchAppointments();
    };

    renderButton() {
        if (this.state.loading) {
            return <Spinner/>
        } else {
            return <Button name={"Buscar Disponibilidad"} light={false} onCLick={this.onClick}/>
        }
    }

    render() {
        const {idCard} = this.state.inputs;
        return (
            <div className={classes.Container}>
                <div className={classes.dates}>
                    <TextInput
                        label={"Cedula"}
                        type={"text"}
                        name={"idCard"}
                        placeholder={"1072669885"}
                        input={idCard} Change={this.handleChange}
                    />
                    <p className={classes.title}>Fecha de Inicio</p>
                    <DatePicker onChange={this.onSelectStart} includeTime/>
                    <p className={classes.title}>Fecha de Cierre</p>
                    <DatePicker onChange={this.onSelectEnd} includeTime/>
                </div>
                <div className={classes.appointmentsContainer}>
                    {this.renderAppointments()}
                </div>
                <div className={classes.buttons}>
                    {this.renderButton()}
                </div>
                {this.renderError()}
            </div>
        )
    }
}


const
    mapDispatchToProps = dispatch => {
        return {
            onAddAppointment: (appointment) => dispatch(actionCreators.addApointmentPatient(appointment)),
            onDeleteAppointment: (appointment) => dispatch(actionCreators.deleteApointmentPatient(appointment)),
            onDeleteAll: () => dispatch(actionCreators.deleteAll())
        }
    };

const
    mapStateToProps = state => {
        return {
            appointments: state.patientStore.appointments,
            user: state.userStore.user
        }
    };

export default connect(mapStateToProps, mapDispatchToProps)(Appointment);
