import React, {Component} from "react"

//own files
import news1 from "./../../assets/Images/news1.jpg"
import news2 from "./../../assets/Images/news2.jpg"
import NewsComponent from "./../../Components/NewsComponent/NewsComponent"
import classes from "./Entrie.css"
import CitiesSlider from "./../../Components/Banner/Banner"

class Entrie extends Component {

    constructor(props) {
        super(props);
        this.state = {
            newsInfo: [
                {
                    image: news1,
                    alt: "Introducing the new system BARBATOS",
                    title: "Introducing Barbatos",
                    text: "The new systema barbatos is the most qualified system to manage all of the medicla issues world wide and its awesomenes is reaching throw all of the world"
                },
                {
                    image: news2,
                    alt: "Easy to make contact",
                    title: "Easy Contact",
                    text: "Now you can asks for appointments easy and fast with the new system, just log in with your credentials and everything will be good to go!"
                }
            ],
            screenWidth: window.innerWidth
        };
    }

    render() {
        const newsInfo = [...this.state.newsInfo]
        let renderContent;
        const slides = [
            {
                city: 'Citas',
                country: 'En Barbatos puedes pedir citas de forma rapida y eficiente',
                img: 'https://wp02-media.cdn.ihealthspot.com/wp-content/uploads/sites/412/2019/03/28194525/HP-Header02-mobile.jpg',
            },
            {
                city: 'Horario',
                country: 'Los medicos tambien pueden revisar las citas de forma clara y facil',
                img: 'https://www.telegraph.co.uk/content/dam/news/2018/10/26/TELEMMGLPICT000124180789-xxlarge_trans_NvBQzQNjv4BqRLU6RByDcehJVbJAoDuCGcdex5NebIP7IPMlx7wGIlk.jpeg',
            },
            {
                city: 'Estadisticas',
                country: 'Revisa las estadísticas de las EPS',
                img: 'https://cdn.lynda.com/course/503930/503930-637009475898890538-16x9.jpg',
            },
            {
                city: 'Contratos',
                country: 'Registra contratos es la plataforma de forma facil y eficiente',
                img: 'https://s3.amazonaws.com/multistate.us/shared/hubspot/export/AdobeStock_67425246-1200px.jpeg',
            },
        ];
        if (this.state.screenWidth > 1024) {
            renderContent = (
                <>
                    <CitiesSlider slides={slides}/>
                    <div className={classes.newsContainer}>
                        {
                            newsInfo.map((item, index) => (
                                <NewsComponent
                                    key={index}
                                    image={item.image}
                                    alt={item.alt}
                                    title={item.title}
                                    text={item.text}
                                    hd={true}
                                />
                            ))
                        }
                    </div>
                </>
            )
        } else {
            renderContent = (
                <>
                    <CitiesSlider slides={slides}/>
                    <div className={classes.newsContainer}>
                        {
                            newsInfo.map((item, index) => (
                                <NewsComponent
                                    key={index}
                                    image={item.image}
                                    alt={item.alt}
                                    title={item.title}
                                    text={item.text}
                                    hd={false}
                                />
                            ))
                        }
                    </div>
                </>
            )
        }
        return (
            renderContent
        )
    }
}

export default Entrie
