import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk'
import {Provider} from 'react-redux';
import patientReducer from "./Store/Reducers/Patient/PatientReducer"
import userReducer from "./Store/Reducers/User/UserReducer"
import DoctorStore from "./Store/Reducers/Doctor/DoctorReducer"

const rootReducer = combineReducers({
    patientStore: patientReducer,
    userStore: userReducer,
    doctorStore: DoctorStore,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));


ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
