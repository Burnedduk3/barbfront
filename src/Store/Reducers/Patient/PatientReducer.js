import {
    DELETEAPPOINTMENT,
    ADDAPPOINTMENT,
} from "./../../ActionTypes/Patient/PatientActionTypes"
import updateObject from "../../utility";
import {DELETEALL} from "../../ActionTypes/Doctor/DoctorActionTypes";


const initial_state = {
    appointments: [],
};

const addApointment = (state, action) => {
    const appointments = [...state.appointments];
    let inArray = false;
    appointments.map((item) => {
        if (item.appointment.calendarId === action.payload.appointment.calendarId) {
            inArray = true;
        }
        return 0
    });
    if (!inArray) {
        appointments.push(action.payload);
    }

    return updateObject(state, {appointments: appointments})
};

const deleteAppointment = (state, action) => {
    const appointments = [...state.appointments];
    let indexOfElement = -1;
    for (let index in appointments) {
        if (appointments[index].appointment.calendarId === action.payload.appointment) {
            indexOfElement = index;
        }
    }
    if (indexOfElement !== -1)
        appointments.splice(indexOfElement, 1);
    return updateObject(state, {...state, appointments: appointments})
};

const deleteAll = (state, action) => {
    return updateObject(state, {appointments: []})
};

const reducer = (state = initial_state, action) => {
    switch (action.type) {

        case ADDAPPOINTMENT:
            return addApointment(state, action);

        case DELETEAPPOINTMENT:
            return deleteAppointment(state, action);

        case DELETEALL:
            return deleteAll(state, action);

        default:
            return state;
    }
};

export default reducer;
