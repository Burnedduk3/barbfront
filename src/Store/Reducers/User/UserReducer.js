import {
    LOGIN,
    LOGOUT
} from "./../../ActionTypes/User/UserActionTypes"
import updateObject from "../../utility";


const initial_state = {
    logedin: false,
    user: null,
};

const login = (state, action) => {
    const logedin = action.payload.requestLogin;
    const user = action.payload.User;
    return updateObject(state, {logedin: logedin, user: user});
};

const logout = (state, action) => {
    const logout = !action.payload;
    return updateObject(state, {logedin: logout, user: null});
};

const reducer = (state = initial_state, action) => {
    switch (action.type) {
        case LOGIN:
            return login(state, action);
        case LOGOUT:
            return logout(state, action);
        default:
            return state;
    }
};

export default reducer;
