import * as actionTypes from "./../../ActionTypes/Doctor/DoctorActionTypes"

export const addApointment = (appointment) =>{
    return {
        type: actionTypes.ADDAPPOINTMENT,
        payload:{
            appointment:appointment
        }
    };
};

export const deleteAppointment = (appointment)=>{
    return{
        type: actionTypes.DELETEAPPOINTMENT,
        payload:{
            appointment: appointment
        }
    }
};

export const deleteAll = ()=>{
    return{
        type: actionTypes.DELETEALL
    }
};
