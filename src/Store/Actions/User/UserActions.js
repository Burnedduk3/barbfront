
import * as actionTypes from "./../../ActionTypes/User/UserActionTypes"

export const login = (requestLogin,User) =>{
    return{
        type: actionTypes.LOGIN,
        payload:{
            requestLogin: requestLogin,
            User: User
        }
    };
};

export const logout = (requestLogOut)=>{
    return{
        type:actionTypes.LOGOUT,
        payload:requestLogOut
    }
};
