import * as actionTypes from "./../../ActionTypes/Patient/PatientActionTypes"

export const addApointmentPatient = (appointment) => {
    return {
        type: actionTypes.ADDAPPOINTMENT,
        payload: {
            appointment: appointment
        }
    };
};

export const deleteApointmentPatient = (appointment) => {
    return {
        type: actionTypes.DELETEAPPOINTMENT,
        payload: {
            appointment: appointment
        }
    }
};

export const deleteAll = () => {
    return {
        type: actionTypes.DELETEALL
    }
};
